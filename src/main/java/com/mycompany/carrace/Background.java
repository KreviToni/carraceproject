/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carrace;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 *
 * @author progmatic
 */
public class Background extends JComponent {

    Image bg;

    public Background() {
        try {
            bg = ImageIO.read(new File("background.png"));
            bg = bg.getScaledInstance(MainFrame.BACKGROUND_SIZE.width, MainFrame.BACKGROUND_SIZE.height, Image.SCALE_DEFAULT);
        } catch (IOException ex) {
            System.out.println("Error reading background image file");
        }
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(bg, 0, 0, null);
    }
}
