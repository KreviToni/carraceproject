/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carrace;

import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 *
 * @author progmatic
 */
public class Sounds {

    private AudioInputStream crashFile, themeSongFile;
    private Clip themeClip;

    public Sounds() {
        try {
            crashFile = AudioSystem.getAudioInputStream(new File("crash.wav").getAbsoluteFile());
            themeSongFile = AudioSystem.getAudioInputStream(new File("themesong.wav").getAbsoluteFile());
        } catch (Exception ex) {
            System.out.println("Error getting sound files.");
            ex.printStackTrace();
        }
    }

    public void playcrashSound() {
        Clip c = null;
        try {
            c = AudioSystem.getClip();
            c.open(crashFile);
        } catch (Exception ex) {
            System.out.println("Error getting sound files.");
            ex.printStackTrace();
        }
        c.loop(1);
    }

    public void playThemeSong() {
        themeClip = null;
        try {
            themeClip = AudioSystem.getClip();
            themeClip.open(themeSongFile);
        } catch (Exception ex) {
            System.out.println("Error getting sound files.");
            ex.printStackTrace();
        }
        themeClip.loop(Clip.LOOP_CONTINUOUSLY);
    }
    
    public void stopThemeSong(){
        themeClip.stop();
    }
}
