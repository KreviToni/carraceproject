/* 
 * To change this license header, choose License Headers in Project Properties. 
 * To change this template file, choose Tools | Templates 
 * and open the template in the editor. 
 */
package com.mycompany.carrace;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 *
 * @author progmatic
 */
public class Car extends JComponent {

    int currentLane;
    Image Car;

    public Car(int lane) {
        currentLane = lane;
        try {
            Car = ImageIO.read(new File("car.png"));
            Car = Car.getScaledInstance(MainFrame.CAR_SIZE.width, MainFrame.CAR_SIZE.height, Image.SCALE_DEFAULT);
        } catch (IOException ex) {
            System.out.println("Error reading mycar image file");
        }
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(Car, 0, 0, null);
    }

    public int getCurrentLane() {
        return currentLane;
    }

}
