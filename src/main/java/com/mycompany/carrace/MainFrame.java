/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.carrace;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 *
 * @author progmatic
 */
public class MainFrame extends JFrame {

    public static final int[] LANES = {20, 140, 260, 380};
    public static final Dimension CAR_SIZE = new Dimension(100, 200);
    public static final Dimension BACKGROUND_SIZE = new Dimension(500, 750);
    private double timeRan;

    MyCar myCar;
    Background bg, bg2;
    Timer backgroundTimer;
    JLabel gameTimeCounter;
    ArrayList<Car> cars = new ArrayList<>();
    JMenuBar menuBar;
    Timer addCarTimer;
    Timer carMovingTimer;
    boolean isOver = false;
    JLabel gO;
    Sounds sounds;
    Timer carLeftTimer, carRightTimer;

    public MainFrame() {
        init();
    }

    private void init() {
        setLayout(null);
        setSize(BACKGROUND_SIZE.width, BACKGROUND_SIZE.height);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setGameTimeCounter();
        sounds = new Sounds();
        createLeftTimer();
        createRightTimer();
        setCars();
        addCars();
        moveCars();
        setMovingBackground();
        setListeners();
        sounds.playThemeSong();
    }

    private void setMovingBackground() {
        bg = new Background();
        bg2 = new Background();
        bg.setBounds(0, 0, BACKGROUND_SIZE.width, BACKGROUND_SIZE.height);
        bg2.setBounds(0, -BACKGROUND_SIZE.height, BACKGROUND_SIZE.width, BACKGROUND_SIZE.height);

        add(bg, 2);
        add(bg2, 2);
        moveBackground();

    }

    private void setGameTimeCounter() {
        Font f = new Font("Serif", Font.BOLD, 24);
        gameTimeCounter = new JLabel();

        gameTimeCounter.setForeground(Color.WHITE);
        gameTimeCounter.setFont(f);

        gameTimeCounter.setBounds(20, 5, 200, 30);
        add(gameTimeCounter, 0);
    }

    private void moveBackground() {
        backgroundTimer = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Toolkit.getDefaultToolkit().sync();

                timeRan += 0.001;
                gameTimeCounter.setText(String.format("Score: %.3f", timeRan));

                bg.setBounds(bg.getX(),
                        bg.getY() == BACKGROUND_SIZE.height ? -BACKGROUND_SIZE.height + 1 : bg.getY() + 1,
                        BACKGROUND_SIZE.width,
                        BACKGROUND_SIZE.height);
                bg2.setBounds(bg2.getX(),
                        bg2.getY() == BACKGROUND_SIZE.height ? -BACKGROUND_SIZE.height + 1 : bg2.getY() + 1,
                        BACKGROUND_SIZE.width,
                        BACKGROUND_SIZE.height);
            }
        });

        backgroundTimer.start();
    }

    private void setCars() {
        myCar = new MyCar();
        myCar.setBounds(LANES[1], BACKGROUND_SIZE.height - (CAR_SIZE.height + 40), CAR_SIZE.width, CAR_SIZE.height);
        add(myCar, 1);
    }

    private void addCars() {
        addCarTimer = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Set<Integer> takenLanes = new HashSet<>();
                for (Car car : cars) {
                    takenLanes.add(car.getCurrentLane());

                }
                if (takenLanes.size() < 3) {
                    double random = Math.random();
                    if (random < 0.65) {
                        int randomLane = (int) (Math.random() * 4);
                        Car carToAdd = new Car(randomLane);
                        carToAdd.setBounds(LANES[randomLane], 0 - CAR_SIZE.height, CAR_SIZE.width, CAR_SIZE.height);
                        boolean isAddable = true;
                        for (Car car : cars) {
                            if (carToAdd.getBounds().intersects(car.getBounds())) {
                                isAddable = false;
                            }
                        }
                        if (isAddable) {
                            add(carToAdd, 1);
                            cars.add(carToAdd);
                        }
                    }
                }
                takenLanes.clear();

            }
        });

        addCarTimer.start();
    }

    private void moveCars() {
        carMovingTimer = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<Car> carsToRemove = new ArrayList<>();
                Toolkit.getDefaultToolkit().sync();
                for (Car car : cars) {
                    if (myCar.getBounds().intersects(car.getBounds())) {
                        isOver = true;
                        backgroundTimer.stop();
                        addCarTimer.stop();
                        carMovingTimer.stop();
                        Font f = new Font("Serif", Font.BOLD, 42);
                        gO = new JLabel("GAME OVER");
                        gO.setFont(f);
                        gO.setForeground(Color.red);
                        gO.setBounds(BACKGROUND_SIZE.width / 2 - 150, BACKGROUND_SIZE.height / 2 - 150, 300, 250);
                        getContentPane().add(gO, 0);
                        sounds.playcrashSound();
                        sounds.stopThemeSong();

                        repaint();
                    }

                    car.setBounds(car.getX(), car.getY() + 1, car.getWidth(), car.getHeight());
                    if (car.getBounds().getY() == BACKGROUND_SIZE.height) {
                        carsToRemove.add(car);
                    }
                }
                for (Car car : carsToRemove) {
                    cars.remove(car);
                }
            }

        });
        carMovingTimer.start();
    }

    private void createLeftTimer() {
        carLeftTimer = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (myCar.getX() > 10) {
                    myCar.setBounds(myCar.getX() - 1, myCar.getY(), myCar.getWidth(), myCar.getHeight());
                }
            }
        });
    }

    private void createRightTimer() {
        carRightTimer = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (myCar.getX() < BACKGROUND_SIZE.width - CAR_SIZE.width - 10) {
                    myCar.setBounds(myCar.getX() + 1, myCar.getY(), myCar.getWidth(), myCar.getHeight());
                }
            }
        });
    }

    private void setListeners() {
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();

                if (backgroundTimer.isRunning()) {
                    if (key == KeyEvent.VK_LEFT && myCar.getX() > 10) {
                        carLeftTimer.start();
                    }
                    if (key == KeyEvent.VK_RIGHT && myCar.getX() < BACKGROUND_SIZE.width - CAR_SIZE.width - 10) {
                        carRightTimer.start();
                    }
                }

                if (key == KeyEvent.VK_SPACE) {
                    if (!isOver) {
                        if (backgroundTimer.isRunning()) {
                            backgroundTimer.stop();
                            addCarTimer.stop();
                            carMovingTimer.stop();

                        } else {
                            backgroundTimer.start();
                            addCarTimer.start();
                            carMovingTimer.start();
                            sounds.playThemeSong();
                        }
                    } else {
                        gameOver();
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                int key = e.getKeyCode();

                if (key == KeyEvent.VK_LEFT) {
                    carLeftTimer.stop();
                }

                if (key == KeyEvent.VK_RIGHT) {
                    carRightTimer.stop();
                }

            }
        });
    }

    public static void main(String[] args) throws InterruptedException {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame mf = new MainFrame();
                mf.setVisible(true);
            }
        });
    }

    private void gameOver() {
        isOver = false;
        for (Car c : cars) {
            this.remove(c);
        }
        cars.clear();
        timeRan = 0;
        this.remove(gO);
        backgroundTimer.restart();
        addCarTimer.restart();
        carMovingTimer.restart();
    }
}
